const Musica = {

    ligarDesligar: function () {
        const music = document.getElementById('audio')
        const bkgMusic = new Audio("./Folders/Songs/background-sound.mp3")

        music.addEventListener('click', function () {

            if (music.classList.contains('off')) {
                bkgMusic.play()
            } else if (music.classList.contains('on')) {
                bkgMusic.pause()
            }
            music.classList.toggle('off')
            music.classList.toggle('on')
        })
    }
}
window.addEventListener('DOMContentLoaded', Musica.ligarDesligar)