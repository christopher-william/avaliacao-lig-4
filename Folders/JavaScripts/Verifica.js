const Verifica = {

    direcoesDeVitoria: function (jogador) {
        const tr = document.getElementsByTagName('tr');

        for (let coluna = 0; coluna < tr.length; coluna++) {
            for (let celula = 0; celula < tr[coluna].children.length; celula++) {

                if (this.vertical(tr, coluna, celula, jogador) 
                    ||  this.horizontal(tr, coluna, celula, jogador) 
                    ||  this.diagonalDireita(tr, coluna, celula, jogador) 
                    ||  this.diagonalEsquerda(tr, coluna, celula, jogador)) {
                    return true;
                }
            }
        }
        return false
    },

    vertical: function (tr, coluna, celula, jogador) {

        if (tr[coluna].children[celula + 3] !== undefined

            && tr[coluna].children[celula + 0].classList.contains(jogador)
            && tr[coluna].children[celula + 1].classList.contains(jogador)
            && tr[coluna].children[celula + 2].classList.contains(jogador)
            && tr[coluna].children[celula + 3].classList.contains(jogador)) {
            return true;
        } else {
            return false
        }
    },

    horizontal: function (tr, coluna, celula, jogador) {

        if (tr[coluna + 3] !== undefined

            && tr[coluna + 0].children[celula].classList.contains(jogador)
            && tr[coluna + 1].children[celula].classList.contains(jogador)
            && tr[coluna + 2].children[celula].classList.contains(jogador)
            && tr[coluna + 3].children[celula].classList.contains(jogador)) {
            return true;
        } else {
            return false;
        }
    },

    diagonalDireita: function (tr, coluna, celula, jogador) {

        if (tr[coluna + 3] !== undefined && tr[coluna].children[celula + 3] !== undefined

            && tr[coluna + 0].children[celula + 0].classList.contains(jogador)
            && tr[coluna + 1].children[celula + 1].classList.contains(jogador)
            && tr[coluna + 2].children[celula + 2].classList.contains(jogador)
            && tr[coluna + 3].children[celula + 3].classList.contains(jogador)) {
            return true;
        } else {
            return false;
        }
    },

    diagonalEsquerda: function (tr, coluna, celula, jogador) {

        if (tr[coluna - 3] !== undefined && tr[coluna].children[celula + 3] !== undefined

            && tr[coluna - 0].children[celula + 0].classList.contains(jogador)
            && tr[coluna - 1].children[celula + 1].classList.contains(jogador)
            && tr[coluna - 2].children[celula + 2].classList.contains(jogador)
            && tr[coluna - 3].children[celula + 3].classList.contains(jogador)) {
            return true;
        } else {
            return false;
        }
    },

    CelulasOcupadas: 0,

    espacosVaziosNaTabela: function () {
        const linhas = document.getElementsByTagName('tr');
        this.CelulasOcupadas = 0

        for (let i = 0; i < linhas.length; i++) {
            for (let j = 0; j < linhas[i].children.length; j++) {

                if (!linhas[i].children[j].classList.contains('default')) {
                    this.CelulasOcupadas++
                }
            }
        }
    },

    empate: function () {
        const quantidadeDeCelulasNaTabela = document.querySelectorAll('td').length
        this.espacosVaziosNaTabela()

        if (this.CelulasOcupadas === quantidadeDeCelulasNaTabela) {
            return true
        } else {
            return false
        }
    },
}