const Player = {

    mudarTurno: function (jogador) {
        jogadorAtual = proximoJogador;
        proximoJogador = jogador;
        this.trocarLeds()
    },

    trocarLeds: function () {
        const led1 = document.getElementById('led1')
        const led2 = document.getElementById('led2')
        led1.classList.toggle('selected')
        led2.classList.toggle('selected')
    },

    default: function () {
        if (jogadorAtual === "Player2") {
            this.mudarTurno(jogadorAtual)
        }
    },
}