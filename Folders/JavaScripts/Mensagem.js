
const Mensagem = {

  vitoria: function (jogador) {
    this.criaJanelaDe(`Vitória do ${jogador}`)
  },

  empate: function () {
    this.criaJanelaDe('Empate')
  },

  criaJanelaDe: function (mensagem) {
    const tabela = document.getElementById('tabela_do_jogo')
    tabela.id = "fimDeJogo"; const fimDeJogo = document.getElementById('fimDeJogo')
    fimDeJogo.innerText = mensagem;
    fimDeJogo.addEventListener("click", NovoJogo.criar);
  }
}

const NovoJogo = {

  criar: function () {
    const board = document.getElementById('board-lig-4')
    const fimDeJogo = document.getElementById('fimDeJogo')
    board.removeChild(fimDeJogo)
    Lig4.main()
    Player.default()
  }

}
