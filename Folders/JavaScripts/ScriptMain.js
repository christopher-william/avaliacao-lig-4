let jogadorAtual = "Player1", proximoJogador = "Player2"

const Lig4 = {

    criaTabela: function () {
        const mesaDeJogo = document.getElementById("board-lig-4")
        const minhaTabela = document.createElement("table");
        minhaTabela.id = "tabela_do_jogo";

        for (let colunas = 0; colunas < 6; colunas++) {
            const tr = document.createElement("tr");
            tr.classList.add("column");

            for (let celulas = 0; celulas < 7; celulas++) {
                const td = document.createElement("td");
                td.classList.add("default");
                tr.appendChild(td);
            }
            minhaTabela.appendChild(tr);
        }
        mesaDeJogo.appendChild(minhaTabela);
    },

    criaMenu: function () {
        const tabela_do_jogo = document.getElementById('tabela_do_jogo')
        const caption = document.createElement('caption')
        const p = document.createElement('p');

        p.innerHTML = 'LIG-4'
        p.id = "game_name";

        caption.appendChild(p);
        tabela_do_jogo.appendChild(caption);
    },

    main: function () {
        this.criaTabela(), this.criaMenu()
        const tabela_do_jogo = document.getElementById('tabela_do_jogo')
        tabela_do_jogo.addEventListener("click", this.StartGame)
    },

    StartGame: function (click) {
        const jogador = jogadorAtual;

        if (Disco.podeAdicionar(click)) {
            Disco.adicionar(jogador)

            if (Verifica.direcoesDeVitoria(jogador)) {
                Mensagem.vitoria(jogador)
            } else if (Verifica.empate()) {
                Mensagem.empate()
            } else {
                Player.mudarTurno(jogador)
            }
        }
    }
}
Lig4.main()