const Disco = {

    selecionado: '',

    podeAdicionar: function (click) {

        for (let index = click.path[1].childElementCount - 1; index >= 0; index--) {

            if (click.path[1].children[index].classList.contains("default")) {
                this.selecionado = click.path[1].children[index]
                return true
            }
        }
        return false
    },

    adicionar: function (jogador) {
        this.selecionado.classList.remove('default')
        this.selecionado.classList.add(jogador);
    }
}